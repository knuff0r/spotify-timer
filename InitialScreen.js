
import React, { Component } from 'react';
import {
	ActivityIndicator,
	Alert,
	StyleSheet,
	Text,
	TouchableHighlight,
	View
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Spotify from 'react-native-spotify';

export class InitialScreen extends Component
{
	static navigationOptions = {
		header: null
	};

	constructor()
	{
		super();

		this.state = {spotifyInitialized: false};
		this.spotifyLoginButtonWasPressed = this.spotifyLoginButtonWasPressed.bind(this);
	}

	goToPlayer()
	{
		const navAction = NavigationActions.reset({
			index: 0,
			actions: [
			  NavigationActions.navigate({ routeName: 'player'})
			]
		});
		this.props.navigation.dispatch(navAction);
	}

	componentDidMount()
	{
		if(!Spotify.isInitialized())
		{
		    console.log("initializing spotify");
			//initialize spotify
			var spotifyOptions = {
				"clientID":"ec055f1841be4245b03a12aa7a6c445a",
				"sessionUserDefaultsKey":"SpotifySession",
				//"redirectURL":"examplespotifyapp://auth",
                "redirectURL":"http://localhost/callback",
				"scopes":["user-read-private","user-read-playback-state", "user-modify-playback-state"],
				//"tokenSwapURL":"http://192.168.0.100:3000/swap",
                //"tokenRefreshURL":"http://192.168.0.100:3000/refresh",
			};
			Spotify.initialize(spotifyOptions, (loggedIn, error) => {
				if(error != null)
				{
					Alert.alert("Error", error.message);
				}
				//update UI state
				this.setState((state) => {
					state.spotifyInitialized = true;
					return state;
				});
				//handle initialization
				if(loggedIn)
                //if(Spotify.isLoggedIn())
				{
					this.goToPlayer();
				}
                else {
                }
			});
		}
		else
		{
			//update UI state
			this.setState((state) => {
				state.spotifyInitialized = true;
				return state;
			});
			//handle logged in
			if(Spotify.isLoggedIn())
			{
				this.goToPlayer();
			}
		}
	}

	spotifyLoginButtonWasPressed()
	{
		Spotify.login((loggedIn, error) => {
			if(error)
			{
				Alert.alert("Error", error.message);
			}
			if(loggedIn)
			{
				this.goToPlayer();
			}
		});
	}


	render()
	{
		if(!this.state.spotifyInitialized)
		{
			return (
				<View style={styles.container}>
					<ActivityIndicator animating={true} style={styles.loadIndicator}>
					</ActivityIndicator>
					<Text style={styles.loadMessage}>
						Loading...
					</Text>
				</View>
			);
		}
		else
		{
			return (
				<View style={styles.container}>
					<TouchableHighlight onPress={this.spotifyLoginButtonWasPressed} style={styles.spotifyLoginButton}>
						<Text style={styles.spotifyLoginButtonText}>Log into Spotify</Text>
					</TouchableHighlight>
				</View>
			);
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},

	loadIndicator: {
		//
	},
	loadMessage: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},

	spotifyLoginButton: {
		justifyContent: 'center',
		borderRadius: 18,
		backgroundColor: 'green',
		overflow: 'hidden',
		width: 200,
		height: 40,
		margin: 20,
	},
	spotifyLoginButtonText: {
		fontSize: 20,
		textAlign: 'center',
		color: 'white',
	},

	greeting: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
});

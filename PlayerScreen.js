import React, {Component} from 'react';
import {
    Alert, Picker,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import {NavigationActions} from 'react-navigation';
import Spotify from 'react-native-spotify';

export class PlayerScreen extends Component {
    static navigationOptions = {
        title: 'Timer',
    };

    constructor() {
        super();

        this.state = {
            spotifyUserName: null,
            devices: null,
            device: null,
            timerPicked: "15",
            timerCount: 15 * 60,
            //intervalId: null
        };

        this.spotifyLogoutButtonWasPressed = this.spotifyLogoutButtonWasPressed.bind(this);
    }

    componentDidMount() {
        Spotify.getMe((result, error) => {
            if (error) {
                Alert.alert("Error Sending the getMe request", error.message);
            }
            else {
                this.setState((state) => {
                    state.spotifyUserName = result.id;
                    return state;
                });
            }
        });
        Spotify.sendRequest("v1/me/player/devices", "GET", null, false, (result, error) => {
            if (error) {
                console.log(error);
            }
            else {
                this.setState((state) => {
                    state.devices = result.devices;
                    state.device = state.devices[0];
                    return state;
                });
            }
        });
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    componentDidUpdate() {
        console.log("update");
    }


    goToInitialScreen() {
        const navAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'initial'})
            ]
        });
        this.props.navigation.dispatch(navAction);
    }

    spotifyLogoutButtonWasPressed() {
        Spotify.logout((error) => {
            if (error) {
                Alert.alert("Error", error.message);
            }
            else {
                this.goToInitialScreen();
            }
        });
    }

    spotifyTogglePressed(initialCount) {

        if (!this.intervalId) {
            counter = initialCount;
            this.intervalId = setInterval(() => {
                let newCount = counter - 1;
                if (newCount >= 0) {
                    counter = newCount;
                    this.setState({timerCount: newCount});
                } else {
                    clearInterval(this.intervalId);
                    this.setState({timerCount: parseInt(this.state.timerPicked) * 60});
                    let params = {
                        "device_id": this.state.device.id,
                    };
                    Spotify.login((loggedIn, error) => {
                        if (error) {
                            Alert.alert("Error", error.message);
                        }
                        if (loggedIn) {
                            Spotify.sendRequest("v1/me/player/pause", "PUT", params, false, (result, error) => {
                                if (error) {
                                    Alert.alert("Error", error.message);
                                }
                            });
                        }
                    });
                }
            }, 1000);
        }
        //this.setState({intervalId: intervalId});

        /*
        let params = {
            "device_id": "db043b2055cb3a47b2eb0b5aebf4e114a8c24a5a",
        };


        */

        /*
        Spotify.playURI("spotify:track:4hlBolXqIsRs6U5V6eLWAV", 0,0, (error)=>{
            if(error) {
                console.log(error);
            }
        });

        Spotify.setPlaying(true, (error)=>{
            if(error) {
                console.log(error);
            }
        });
        */

    }

    renderUserName() {
        if (this.state.spotifyUserName != null) {
            return (
                <Text style={styles.greeting}>
                    You are logged in as {this.state.spotifyUserName}
                </Text>
            );
        }
        else {
            return (
                <Text style={styles.greeting}>
                    Getting user info...
                </Text>
            );
        }
    }

    renderDevices() {
        if (this.state.devices != null) {
            return (
                <Picker
                    style={{width: 200}}
                    selectedValue={this.state.device.id}
                    onValueChange={(itemValue, itemPosition) => this.setState({device: this.state.devices[itemPosition]})}>
                    {
                        this.state.devices.map((device, index) => {
                            return <Picker.Item key={index} value={device.id} label={device.name}/>
                        })
                    }
                </Picker>
            );
        }
        else {
            return (
                <Text style={styles.greeting}>
                    Loading devices...
                </Text>
            );
        }
    }

    renderTimerPicker() {
        return (
            <Picker
                style={{width: 200}}
                selectedValue={this.state.timerPicked}
                onValueChange={(itemValue, itemIndex) => this.setState({
                    timerPicked: itemValue,
                    timerCount: parseInt(itemValue) * 60
                })}>
                <Picker.Item label="1 min" value="1"/>
                <Picker.Item label="5 min" value="5"/>
                <Picker.Item label="10 min" value="10"/>
                <Picker.Item label="15 min" value="15"/>
                <Picker.Item label="20 min" value="20"/>
                <Picker.Item label="30 min" value="30"/>
                <Picker.Item label="60 min" value="60"/>
            </Picker>
        );
    }


    render() {
        return (
            <View style={styles.container}>
                {this.renderUserName()}
                <TouchableHighlight onPress={this.spotifyLogoutButtonWasPressed} style={styles.spotifyLogoutButton}>
                    <Text style={styles.spotifyLoginButtonText}>Logout</Text>
                </TouchableHighlight>

                <Text>Select Device</Text>
                {this.renderDevices()}

                <Text>Select Countdown</Text>
                {this.renderTimerPicker()}


                <TouchableHighlight onPress={() => this.spotifyTogglePressed(this.state.timerCount)}
                                    style={styles.spotifyLoginButton}>
                    <Text style={styles.spotifyLoginButtonText}>Start Timer</Text>
                </TouchableHighlight>

                <Text style={styles.countdown}>{this.state.timerCount}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    greeting: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    spotifyLoginButton: {
        justifyContent: 'center',
        borderRadius: 18,
        backgroundColor: 'green',
        overflow: 'hidden',
        width: 200,
        height: 40,
        margin: 20,
    },
    spotifyLoginButtonText: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
    },
    spotifyLogoutButton: {
        justifyContent: 'center',
        borderRadius: 18,
        backgroundColor: '#1a1c1d',
        overflow: 'hidden',
        width: 200,
        height: 40,
        margin: 20,
    },
    countdown: {
        fontSize: 28,
    }
});
